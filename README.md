# Foundry VTT Dice So Nice Tests

This repository contains the GUI tests written with [Playwright](https://playwright.dev) for [Foundry VTT Dice So Nice](https://gitlab.com/riccisi/foundryvtt-dice-so-nice)

These tests can only work on JDW's local machine.  

This repository is public to allow others to look at a Playwright integration example for a Foundry module/system.

## Running the tests
`npx playwright test`