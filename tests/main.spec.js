const { test, expect } = require('@playwright/test');
let page;

//Test basic DsN stuff
test.describe('Testing main dsn features', () => {

	test.beforeAll(async ({ browser }) => {
		const context = await browser.newContext({
			viewport: {
				width: 1920,
				height: 1080
			}
		});
		page = await context.newPage();

		page.on('console', msg => {
			if (msg.type() === 'error')
				console.log(`Error text: "${msg.text()}"`);
		});

		await initializeWorldWithDsN(page, 'playwrightdnd', 'bzdmrFyixztV2lkB');
	});

	test.afterAll(async ({ browser }) => {
		await page.evaluate(async () => {
			await game.shutDown();
		});
	});

	
	/**
	 * Testing special effects:
	 * White Glow
	 * Darkness
	 * Glass Impact
	 * Thormund
	 * Blaze
	 * Magic Vortex
	 * Epic Fail
	 * Epic Win
	 * Custom sound
	 * Custom macro
	 */
	test('testing rolls with multiple core themes', async ({ }, testInfo) => {
		await loadSaveFile(page, "save1");
		await executeFormulaAndWait(page, "d2+d3+d4+d5+d8+d100+d12+d20+dc+df");
		const screenshot = await page.screenshot({ path: './test-results/test_rolls_with_multiple_core_themes.png' });
		await testInfo.attach('screenshot', { body: screenshot, contentType: 'image/png' });
	});

	test('testing special effects: White Glow', async ({ }, testInfo) => {
		await loadSaveFile(page, "savefx");
		await executeFormulaAndWait(page, "d2");
		await page.waitForTimeout(400);
		const screenshot = await page.screenshot({ path: './test-results/test_special_effects_white_glow.png' });
		await testInfo.attach('screenshot', { body: screenshot, contentType: 'image/png' });
	});

	test('testing special effects: Darkness', async ({ }, testInfo) => {
		await executeFormulaAndWait(page, "dc");
		await page.waitForTimeout(500);
		const screenshot = await page.screenshot({ path: './test-results/test_special_effects_darkness.png' });
		await testInfo.attach('screenshot', { body: screenshot, contentType: 'image/png' });
	});

	test('testing special effects: Glass Impact', async ({ }, testInfo) => {
		await executeFormulaAndWait(page, "d4");
		await page.waitForTimeout(1000);
		const screenshot = await page.screenshot({ path: './test-results/test_special_effects_glass_impact.png' });
		await testInfo.attach('screenshot', { body: screenshot, contentType: 'image/png' });
	});

	test('testing special effects: Thormund', async ({ }, testInfo) => {
		await executeFormulaAndWait(page, "d6");
		await page.waitForTimeout(1000);
		const screenshot = await page.screenshot({ path: './test-results/test_special_effects_thormund.png' });
		await testInfo.attach('screenshot', { body: screenshot, contentType: 'image/png' });
	});

	test('testing special effects: Blaze', async ({ }, testInfo) => {
		await executeFormulaAndWait(page, "df");
		await page.waitForTimeout(600);
		const screenshot = await page.screenshot({ path: './test-results/test_special_effects_blaze.png' });
		await testInfo.attach('screenshot', { body: screenshot, contentType: 'image/png' });
	});

	test('testing special effects: Magic Vortex', async ({ }, testInfo) => {
		await executeFormulaAndWait(page, "d8");
		await page.waitForTimeout(600);
		const screenshot = await page.screenshot({ path: './test-results/test_special_effects_magic_vortex.png' });
		await testInfo.attach('screenshot', { body: screenshot, contentType: 'image/png' });
	});

	test('testing special effects: Epic Fail', async () => {
		await executeFormulaAndWait(page, "d10");
		await page.waitForTimeout(500);
	});

	test('testing special effects: Epic Win', async () => {
		await executeFormulaAndWait(page, "d3");
		await page.waitForTimeout(500);
		expect(true).toBeTruthy();
	});

	test('testing special effects: Custom sound', async () => {
		await executeFormulaWithResultAndWait(page, "d100", 100);
		await page.waitForTimeout(500);
		expect(true).toBeTruthy();
	});

	test('testing special effects: Custom macro', async () => {
		await executeFormulaAndWait(page, "d12");
		await expect(page.locator("#chat .chat-message:last-child .message-content")).toHaveText('Macro SFX OK');
	});

	test('testing force appearance (fire + ice + acid)', async ({ }, testInfo) => {
		await page.evaluate(async () => {
			await game.dice3d.box.clearAll();
			//Test flavor
			let r;
			r = await new Roll("d20[fire]").evaluate();
			r.toMessage();

			//Test force colorset
			r = await new Roll("1d20").evaluate();
			r.dice[0].options.appearance = { colorset: "ice" };
			r.toMessage();


			//test force appearance
			r = await new Roll("d20").evaluate();
			r.dice[0].options.appearance = {
				colorset: "custom",
				foreground: "#FFFFFF",
				background: "#FF0000",
				outline: "#000000",
				edge: "#000000",
				texture: "acid",
				material: "metal",
				font: "Arial Black",
				system: "standard"
			};
			let msg = await r.toMessage();
			await game.dice3d.waitFor3DAnimationByMessageID(msg.id);
		});
		const screenshot = await page.screenshot({ path: './test-results/test_force_appearance.png' });
		await testInfo.attach('screenshot', { body: screenshot, contentType: 'image/png' });
	});

	test('testing roll order from chat message', async ({ }, testInfo) => {
		await loadSaveFile(page, "savefx");
		console.log("Should roll d20 first, then d6 and d4");
		await page.evaluate(async () => {
			await game.dice3d.box.clearAll();
			let attack = await new Roll("d20").evaluate({ async: true });
			attack.dice[0].options.rollOrder = 1;

			let directDamage = await new Roll("d6").evaluate({ async: true });
			directDamage.dice[0].options.rollOrder = 2;

			let aoeDamage = await new Roll("d4").evaluate({ async: true });
			aoeDamage.dice[0].options.rollOrder = 2;

			//Merge rolls
			const rolls = [attack, directDamage, aoeDamage]; //array of Roll
			const pool = PoolTerm.fromRolls(rolls);
			roll = Roll.fromTerms([pool]);

			let msg = await roll.toMessage();
			await game.dice3d.waitFor3DAnimationByMessageID(msg.id);
		});

		const screenshot = await page.screenshot({ path: './test-results/test_roll_order_from_chat_message.png' });
		await testInfo.attach('screenshot', { body: screenshot, contentType: 'image/png' });
	});

	test('testing chat message with multiple rolls attached', async ({ }, testInfo) => {
		await page.evaluate(async () => {
			await game.dice3d.box.clearAll();
			let r1 = await new Roll("1d20").evaluate();
			let r2 = await new Roll("1d6").evaluate();
			let chatOptions = {
				type: CONST.CHAT_MESSAGE_TYPES.ROLL,
				rolls: [r1,r2],
				rollMode: game.settings.get("core", "rollMode"),
				content: '<p>Testing multiple roll in chat message</p>'
			};
			let msg = await ChatMessage.create(chatOptions);
			await game.dice3d.waitFor3DAnimationByMessageID(msg.id);
		});
		const screenshot = await page.screenshot({ path: './test-results/test_roll_order_from_chat_message.png' });
		await testInfo.attach('screenshot', { body: screenshot, contentType: 'image/png' });
	});
});

test.describe('Test SWFFG dice', () => {
	test.beforeAll(async ({ browser }) => {
		await initializeWorldWithDsN(page, 'playwrightswffg', 'JFFhrbXcKqqshT8N');
	});

	test.afterAll(async ({ browser }) => {
		await page.evaluate(async () => {
			await game.shutDown();
		});
	});

	test('testing SWFFG dice', async ({ }, testInfo) => {
		await executeFormulaAndWait(page, "dc+df+da+db+di+dp+ds");
		const screenshot = await page.screenshot({ path: './test-results/test_swffg_dice.png' });
		await testInfo.attach('screenshot', { body: screenshot, contentType: 'image/png' });
	});
});

test.describe('Test SWADE bennies and dsn integration', () => {
	test.beforeAll(async ({ browser }) => {
		await initializeWorldWithDsN(page, 'playwrightswade', 'XoPyJBY0M8KRC1fK');
	});

	test.afterAll(async ({ browser }) => {
		await page.evaluate(async () => {
			await game.shutDown();
		});
		browser.close;
	});

	test('testing benny', async ({ }, testInfo) => {
		await executeFormulaAndWait(page, "db");
		const screenshot = await page.screenshot({ path: './test-results/test_benny.png' });
		await testInfo.attach('screenshot', { body: screenshot, contentType: 'image/png' });
	});

	test('testing wild dice', async ({ }, testInfo) => {
		await page.evaluate(async () => {
			await game.dice3d.box.clearAll();
			let r = Roll.fromJSON('{"class":"Roll","options":{},"dice":[],"formula":"{1d4x[Athletics],1d6x[Wild Die]}kh","terms":[{"class":"PoolTerm","options":{},"evaluated":true,"terms":["1d4x[Athletics]","1d6x[Wild Die]"],"modifiers":["kh"],"rolls":[{"class":"Roll","options":{},"dice":[],"formula":"","terms":[{"class":"Die","options":{"flavor":"Athletics"},"evaluated":true,"number":1,"faces":4,"modifiers":["x"],"results":[{"result":1,"active":true}]}],"total":1,"evaluated":true},{"class":"Roll","options":{},"dice":[],"formula":"","terms":[{"class":"Die","options":{"flavor":"Wild Die","colorset":"cold"},"evaluated":true,"number":1,"faces":6,"modifiers":["x"],"results":[{"result":4,"active":true}]}],"total":4,"evaluated":true}],"results":[{"result":1,"active":false,"discarded":true},{"result":4,"active":true}]}],"total":4,"evaluated":true}');
			let msg = await r.toMessage();
			await game.dice3d.waitFor3DAnimationByMessageID(msg.id);
		});
		const screenshot = await page.screenshot({ path: './test-results/test_wild_dice.png' });
		await testInfo.attach('screenshot', { body: screenshot, contentType: 'image/png' });
	});
});

async function initializeWorldWithDsN(page, worldid, userid){
	// Go to http://localhost:30000/setup
	await page.goto('http://localhost:30000/setup', { waitUntil: 'networkidle' });

	if (page.url() === 'http://localhost:30000/setup') {
		await Promise.all([
			page.waitForNavigation(/*{ url: 'http://localhost:30000/join' }*/),
			page.locator(`[data-package-id="${worldid}"] a.play`).dispatchEvent('click')
		]);
	}

	// Select bzdmrFyixztV2lkB
	await page.locator('select[name="userid"]').focus();
	await page.locator('select[name="userid"]').selectOption(userid);

	// Click button:has-text("Join Game Session")
	await Promise.all([
		page.waitForNavigation(/*{ url: 'http://localhost:30000/game' }*/),
		page.locator('button:has-text("Join Game Session")').click({ force: true })
	]);

	//Wait for Dice So Nice ready, when game.dice3d.queue is an empty array
	await page.waitForFunction(() => {
		return game?.dice3d?.queue?.length === 0;
	});

	//Give some breathing time to Foundry to minimize dropped frames
	await page.waitForTimeout(2000);
	page.evaluate(() => window.ui.sidebar.expand());
}

async function executeFormulaAndWait(page, formula) {
	await page.evaluate(async formula => {
		await game.dice3d.box.clearAll();
		let r = await new Roll(formula).evaluate();
		let msg = await r.toMessage();
		await game.dice3d.waitFor3DAnimationByMessageID(msg.id);
	}, formula);
}

async function executeFormulaWithResultAndWait(page, formula, result) {
	const data = { formula: formula, result: result };
	await page.evaluate(async (data) => {
		await game.dice3d.box.clearAll();
		let r = await new Roll(data.formula).evaluate();
		r.dice[0].results[0].result = data.result;
		let msg = await r.toMessage();
		await game.dice3d.waitFor3DAnimationByMessageID(msg.id);
	}, data);
}

async function loadSaveFile(page, savename) {
	await page.evaluate(async savename => {
		await game.dice3d.loadSaveFile(savename);
	}, savename);
}